package com.festo.springschool.warehouse.cli;

import com.beust.jcommander.JCommander;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommandLineHandler {

    private static final Logger LOG = LoggerFactory.getLogger(CommandLineHandler.class);

    private JCommander jCommander;

    public void handle(final String... args) {
        System.out.println(String.format("Received %d arguments: [%s]", args.length, String.join(",", args)));
        LOG.info("This is a test log message");
        jCommander.usage();
        System.exit(0); // NOSONAR
    }

    public void setJCommander(final JCommander jCommander) {
        this.jCommander = jCommander;
    }

}
